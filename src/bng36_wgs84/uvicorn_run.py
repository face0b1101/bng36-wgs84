import uvicorn

# from . import main


def main():
    # logging.basicConfig(
    #     format="{levelname:7} {message}", style="{", level=logging.DEBUG
    # )
    uvicorn.run(
        "bng36_wgs84.main:app",
        host="0.0.0.0",
        port=80,
        reload=True,
    )
