import logging

from typing import Tuple, List
from os import environ
from os.path import join, dirname, abspath

from dotenv import load_dotenv

from fastapi import FastAPI
from fastapi.logger import logger

from pydantic import BaseModel

from convertbng.util import convert_bng, convert_lonlat

app_dir = dirname(abspath(dirname(abspath(__file__))))

dotenv_path = join(app_dir, ".env")
load_dotenv(dotenv_path)

LOGSTASH_ADDR = environ.get("LOGSTASH_ADDR")
LOG_LEVEL = environ.get("LOG_LEVEL")
JSON_LOGS = environ.get("JSON_LOGS")

# Set log level if specified in .env (default is WARNING)
if LOG_LEVEL:
    logging.basicConfig(level=LOG_LEVEL.upper())
else:
    logging.basicConfig(level="DEBUG")

app = FastAPI(prefix="/", responses={404: {"description": "Not found"}})


class BNG_36_Coordinates(BaseModel):
    eastings: float
    northings: float

    class Config:
        schema_extra = {"example": {"eastings": 529155.561, "northings": 179736.441}}


class WGS_84_Coordinates(BaseModel):
    lat: float
    lon: float

    class Config:
        schema_extra = {"example": {"lat": 51.501817, "lon": -0.14058530}}


@app.on_event("startup")
async def startup_event():
    logging.info(f"LOG_LEVEL: {LOG_LEVEL}")
    logging.debug(f"App Dir is: {app_dir}")
    logging.debug(f".env path is: {dotenv_path}")
    logging.debug(f"LOGSTASH_ADDR is: {LOGSTASH_ADDR}")


# @app.get("/")
# async def root():
#     return {"message": "Nothing to see here, move along"}


# test data:
# (530050.825,179917.158) == Downing Street
# (529155.561,179736.441) == Wedding Cake
@app.post("/convert/bng36", response_model=List[WGS_84_Coordinates])
async def convert_bng36(bng36_coords: List[BNG_36_Coordinates]):
    """
    Takes a list of BNG_36_Coordinates objects, converts them to WGS84 and returns a list.

    Args:
        bng36_coords (List[BNG_36_Coordinates]): List of {eastings: float, northings: float} objects

    Returns:
        List[Dict]: List of Dicts {eastings: float, northings: float}
    """
    bng_list = []
    for x in bng36_coords:
        bng_list.append(tuple((x.eastings, x.northings)))

    eastings, northings = list(zip(*bng_list))
    logging.debug(f"eastings: {eastings}, northings: {northings}")
    wgs84 = convert_lonlat(eastings, northings)
    wgs_ret = []
    for (lat, lon) in zip(wgs84[1], wgs84[0]):
        wgs_ret.append({"lat": lat, "lon": lon})

    return wgs_ret


# test data:
# (-0.12762755,51.503236) == Downing Street
# (-0.14058530,51.501817) == Wedding Cake
@app.post("/convert/wgs84", response_model=List[BNG_36_Coordinates])
async def convert_wgs84(wgs84_coords: List[WGS_84_Coordinates]):
    """
    Takes a list of WGS_84_Coordinates objects, converts them to BNG36 and returns a list.

    Args:
        wgs84_coords (List[WGS_84_Coordinates]): List of {lat: float, lon: float} objects

    Returns:
        List[Dict]: List of Dicts {eastings: float, northings: float}
    """
    wgs_list = []
    for x in wgs84_coords:
        wgs_list.append(tuple((x.lat, x.lon)))

    lat, lon = list(zip(*wgs_list))
    logging.debug(f"lats: {lat}, lons: {lon}")
    bng36 = convert_bng(lon, lat)
    bng_ret = []
    for (eastings, northings) in zip(bng36[0], bng36[1]):
        logging.debug(f"eastings: {eastings}, northings: {northings}")
        bng_ret.append({"eastings": eastings, "northings": northings})

    return bng_ret
