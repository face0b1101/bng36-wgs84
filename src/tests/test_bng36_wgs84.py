from bng36_wgs84 import __version__


def test_version():
    assert __version__ == '0.1.0'
