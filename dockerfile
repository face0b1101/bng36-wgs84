FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

RUN pip install --upgrade pip

RUN apt-get update \
  && apt-get install --no-install-recommends -y \
  curl \
  apt-utils

ARG poetry_version="master"

# install-poetry.py and get-poetry.py are options; but get-poetry.py is deprecated though!
ARG poetry_script_name="install-poetry.py"
ARG debug_poetry="false"
ENV PATH="/opt/poetry/bin:${PATH}"

# Install Poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/${poetry_version}/${poetry_script_name} | \
  POETRY_HOME=/opt/poetry python && \
  cd /usr/local/bin && \
  ln -s /opt/poetry/bin/poetry && \
  poetry config virtualenvs.create false && \
  poetry config virtualenvs.in-project false && \
  poetry config experimental.new-installer false

RUN if [ ${debug_poetry} = "true" ] ; \
  then echo $debug_poetry && \
  apt-get update && \
  apt-get install -y tree && \
  cd /opt && \
  tree -la . ; \
  fi

# Copy using poetry.lock* in case it doesn't exist yet
COPY ./pyproject.toml ./poetry.lock* /app/bng36_wgs84/

WORKDIR /app/bng36_wgs84

# If using get-poetry.py - /opt/poetry/env instead of /opt/poetry/venv
RUN if [ ${poetry_script_name} = "get-poetry.py" ]; \
  then which python && \
  . /opt/poetry/env && \
  poetry install --no-root --no-dev && \
  poetry env info ; \
  fi

# If using install-poetry.py - /opt/poetry/venv instead of /opt/poetry/env
RUN if [ ${debug_poetry} = "true" ] ; \
  then echo $debug_poetry && \
  apt-get update && \
  apt-get install -y tree && \
  cd /usr/local/bin && \
  tree -la . ; \
  fi

RUN if [ ${poetry_script_name} = "install-poetry.py" ]; \
  then poetry export -f requirements.txt --output requirements.txt && \
  pip install --no-cache-dir -r requirements.txt ; \
  fi

# TODO: fix this once https://github.com/python-poetry/poetry/issues/3870 gets resolved
# RUN if [ ${poetry_script_name} = "install-poetry.py" ]; \
#     then which python && cd /usr/local/bin/ && \
#     poetry config --list && poetry config virtualenvs.create false && \
#     cd /app/bng36_wgs84/ && poetry env use `which python` && \
#     poetry install --no-root --no-dev -vvv && \
#     poetry env info ; fi

# Final copy
COPY ./src/bng36_wgs84 /app/bng36_wgs84

# Running pip list below to use output to verify that our pyproject.toml file has been installed correctly
RUN pip list

EXPOSE 80